function doSomething(e) { // e = event
    alert(e.type)
    alert(e.target)
    console.log(`Screen X: ${e.screenX} and Y: ${e.screenY}`)
    console.log(`Client X: ${e.clientX} and Y: ${e.clientY}`)
    console.log(`Page X: ${e.pageX} and Y: ${e.pageY}`)
}

// addEventListener('click', doSomething)

const dblclickParagraph = document.getElementById('dblclick');
dblclickParagraph.addEventListener('dblclick', highlight);

const mouseParagraph = document.getElementById('mouse');
mouseParagraph.addEventListener('mouseover', highlight)
mouseParagraph.addEventListener('mouseout', highlight)
mouseParagraph.addEventListener('mousemove', ()=>console.log('You moved'));

function highlight(event) {
    event.target.classList.toggle('highlight');
}

addEventListener('keydown', (e)=> {
    console.log(`KeyCode: ${e.keyCode}`)
    console.log(`Key: ${e.key}`)
})

// addEventListener('dblclick', doSomething)