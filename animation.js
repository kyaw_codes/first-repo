const square = document.getElementById('square');
let stop = true;
let interval;

const animation = function() {
    let angle = 0;
    interval = setInterval(() => {
        angle = (angle + 2) % 360;
        square.style.transform = `rotate(${angle}deg)`;
    }, 1000 / 40);
}
animation();

square.addEventListener('click', (e) => {
    if (stop) {
        clearInterval(interval);
        stop = false;
    } else {
        animation();
        stop = true;
    }
});