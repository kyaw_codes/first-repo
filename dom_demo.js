const myGround = document.getElementById('playground')
const hello = createElement('h1', 'Hello World');
myGround.appendChild(hello)

const list = document.getElementById('list')
const student_one = createElement('li', 'Ko Kyaw')
list.appendChild(student_one)

function createElement(tag, text) {
    const element = document.createElement(tag);
    element.textContent = text;
    return element;
}

function addMore(value) {
    const std = createElement('li', value);
    list.append(std);
}

function showHide(tag) {
    displayTxt = tag.style.display;

    if (displayTxt == 'none') {
        displayTxt = 'block'
    } else {
        displayTxt = 'none'
    }

    tag.style.display = displayTxt
}