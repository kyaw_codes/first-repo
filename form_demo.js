const form = document.forms[0];
const input = form.elements['searchInput'];

input.setAttribute('placeholder', 'Search')
form.setAttribute('autocomplete', 'off');

form.addEventListener('submit', validate, false);

input.addEventListener('focus', ()=>console.log('you focused!'), false);
input.addEventListener('blur', () => console.log('you blur!'), false);

function search(e) {
    alert(`You search for ${input.value}`);
    e.preventDefault();
}

function validate(e) {
    const name = form.heroName.value;

    if (name[0].toUpperCase() === 'X') {
        e.preventDefault();
        alert('Invalid name start with X!');
    } else {
        e.preventDefault();
        alert('Successful')
    }
}

x = 20;

if (window.x === 20) {
    console.log('Hi X');
}